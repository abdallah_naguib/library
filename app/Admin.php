<?php

namespace App;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * this method returns the books created by this admin
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books(){
        return $this->hasMany(Book::class);
    }

    /**
     * this method returns the categories created by this admin
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categorys(){
        return $this->hasMany(Category::class);
    }

    /**
     * this method adds an admin to the database
     * @param Request $request
     */
    public static function add(Request $request){
        $user = new Admin();
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        // if he provides an image then save it
        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move("uploads/users/",$filename);
            $user->image = $filename;
        }
        $user->access_key = Str::random(50);
        $user->save();
    }
}
