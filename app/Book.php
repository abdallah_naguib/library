<?php

namespace App;
use App\Events\BookFinished;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Book extends Model
{
    protected $fillable = [
        'name', 'description', 'quantity','price','category_id'
    ];

    /**
     * The model's default values for attributes.
     * TODO always remember this
     * @var array
     */
    protected $attributes = [
        'name' => 'default name',
    ];

    /**
     * You can refresh models using the
     * fresh and refresh methods. The fresh
     * method will re-retrieve the model from the database.
     * The existing model instance will not be affected
     * The refresh method will re-hydrate the
     * existing model using fresh data from the database.
     * In addition, all of its loaded relationships will
     * be refreshed as well
     */

    public function doSomething(){
        $book = all()->where('name','abdo')->first();
        $book->name = 'zizo';
        $book->refresh();
        $book->name; // this will return abdo
        $book->name = 'any';
        //book2 name is abdo but book name is still
        $book2 = $book->fresh();

        // this will get all the active books
        $books = Book::all()->reject(function ($book){
            return !$book->active;
        });
        // we can fill the object's data this way
        $book->fill(['name'=>'anyname']);

    }

    protected static function boot(){
        Parent::boot();
        /**
         * now when we call the all the method it will only
         * get books with likes
         */
        static::addGlobalScope('likes',function (Builder $builder){
            $builder->where('likes','>',0);
        });
    }

    public function admin(){
        return $this->belongsTo(Admin::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }

    /**
     * this method returns the comments associated with this
     * book
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    /**
     * this method returns the likes associated with this
     * book
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes(){
        return $this->hasMany(Like::class);
    }
    /**
     * this method returns the orders associated with this
     * book
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(){
        return $this->hasMany(order::class);
    }


    /**
     * this method adds a book to the database
     * @param Request $request
     */
    public static function add(Request $request){
        $book = new Book();
        $book->name = $request['name'];
        $book->description = $request['description'];
        $book->price = $request['price'];
        $book->quantity = $request['quantity'];
        $book->category_id = $request['category'];
        // if the admin provides an image with the book then save it
        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move("uploads/books/",$filename);
            $book->image = $filename;
        }
        $book->admin_id = Auth::user()->id;
        $book->save();
    }
    public function updateQuantity($quantity){
        $this->quantity = $quantity;
        if($quantity==0){
            /*
             * if the book is out then send a mail to the admin telling
             * him to add new books in the stock
             */
            $this->active=false;
            event(new BookFinished($this));
        }
        $this->save();
    }
}
