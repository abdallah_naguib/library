<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Category extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * this method returns the books associated with this
     * category
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books(){
        return $this->hasMany(Book::class);
    }

    /**
     * this method adds a category to the database
     * @param Request $request
     */
    public static function add(Request $request){
        $category = new Category();
        $category->name = $request->get('name');
        $category->admin_id = Auth::user()->id;
        $category->save();
    }
}
