<?php

namespace App\Http\Controllers\APIs;

use App\Http\Resources\AdminsResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
class AdminsAPI extends Controller
{
    public function index(){
        $admins = Admin::all();
        return AdminsResource::collection($admins);
    }
}
