<?php

namespace App\Http\Controllers\APIs;

use App\Category;
use App\Http\Resources\CategoriesResource;
use App\Http\Controllers\Controller;

class CategoriesAPI extends Controller
{
    public function get($admin_id){
        $categories = Category::all()->where('admin_id',$admin_id);
        return CategoriesResource::collection($categories);
    }
}
