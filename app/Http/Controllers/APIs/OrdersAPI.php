<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrdersResource;
use App\Order;

class OrdersAPI extends Controller
{
    public function get($id){
        $orders = Order::all()->where('visitor_id',$id);
        return OrdersResource::collection($orders);
    }
}
