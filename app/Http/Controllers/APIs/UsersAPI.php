<?php

namespace App\Http\Controllers\APIs;

use App\Http\Resources\UsersResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersAPI extends Controller
{
    public function index(){
        $users = User::all();
        return UsersResource::collection($users);
    }
}
