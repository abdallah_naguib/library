<?php

namespace App\Http\Controllers\APIs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class commentsAPI extends Controller
{
    public function index($bookId){
        $statment = "select users.image , users.name , comments.content
                    from comments left join users on 
                    comments.user_id = users.id where 
                    comments.book_id = $bookId";
        $comments=DB::select("$statment",[1]);
        return response()->json($comments);
    }
}
