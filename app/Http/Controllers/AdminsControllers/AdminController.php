<?php

namespace App\Http\Controllers\AdminsControllers;

use App\Admin;
use App\Book;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\AdminAddRequest;
use App\Http\Requests\BookRequests\BookOrderRequest;
use App\Http\Requests\UserRequests\UserUpdateRequest;
use App\Order;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * returns the home view for the admin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return View('admin.home');
    }

    /**
     * returns the view for creating new admins
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addAdmin(){
        return View('admin.admins.create');
    }

    /**
     * returns the view for showing all registered admins
     * and sends a list with admins to the view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdmins(){
        $admins = Admin::all();
        return view('admin.admins.index',['list'=>$admins]);
    }

    /**
     * this method is called when an admin adds another admin
     * so we take the request and validate it , then adding it to the
     * admins table and sends the admin back to the admins view
     * the validation is done in the AdminAddRequest class
     * @param AdminAddRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAdmin(AdminAddRequest $request){
        Admin::add($request);
        return redirect('admin/admins');
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteAdmin($id){
        $user = Admin::findOrFail($id);
        /*
         * when deleting an admin update its books and categories to
         * super admin
         */
        DB::table('books')
            ->where('admin_id', $id)
            ->update(['admin_id'=>1]);

        DB::table('categories')
            ->where('admin_id', $id)
            ->update(['admin_id'=>1]);
        $user->delete();
        return back();
    }

    /**
     * this method sends the admin to edit page to edit some admin
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditAdmin($id){
        $user=Admin::findOrFail($id);
        return view('admin.admins.edit',['admin'=>$user]);
    }

    /**
     * this method updates some admin and
     * @param $id
     * @param UserUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editAdmin($id , UserUpdateRequest $request){
        $pass=$request['password'];
        $user = Admin::findOrFail($id);
        /*
         * if there is a password then update the field
         * if not then don't update it
         */
        if($pass){
            $user->update([
                'name'=>$request['name'],
                'email'=>$request['email'],
                'password'=>bcrypt($pass),
            ]);
        }else{
            $user->update([
                'name'=>$request['name'],
                'email'=>$request['email'],
            ]);
        }
        return redirect('admin/admins');
    }
    public function task(){
        return view('admin.task');
    }
    public function order(BookOrderRequest $request){
      //  dd($request->all());
        $book = Book::find($request->get('books_selector'));
        $book->updateQuantity($book->quantity - $request['quantity']);
        $book->save();
        Order::create(['visitor_id'=>$request->get('visitors_selector'),
                        'book_id'=>$request->get('books_selector'),
                        'quantity'=>$request->get('quantity')]);
        return redirect('admin/task');
    }
    public function active(){
        $request = Request()->all();
        $isActive = $request['isActive'];
        $item = Admin::findOrFail($request['id']);
        $item->active = ($isActive=="true")?1:0;
        $item->save();
        return $item;
    }
}
