<?php

namespace App\Http\Controllers\AdminsControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequests\UserLoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminsAuthController extends Controller
{
    /**
     * returns the login view for the admin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdminLogin(){
        return view('auth.admin_login');
    }

    /**
     * all the validation is done in the UserLoginRequest class
     * @param UserLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAdminLogin(UserLoginRequest $request){
        // attempt to login the admin
        if(Auth::guard('admin')->attempt($request->only('email', 'password'))){
            return redirect('/admin');
        }
        // if fails to to login then send him back to the login page
        // with error message
        Session::flash('error','wrong email or password');
        return redirect('/admin/login')->withInput();
    }
}
