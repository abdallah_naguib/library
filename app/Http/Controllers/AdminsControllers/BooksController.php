<?php

namespace App\Http\Controllers\AdminsControllers;

use App\Book;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequests\BookStoreRequest;
use Illuminate\Support\Facades\DB;

class BooksController extends Controller
{
    /**
     * returns the books view for the admin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBooks(){
        $books = Book::all();
        return view('admin.books.index',['list'=>$books]);
    }

    /**
     * this sends the user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addBook(){
        $categories = Category::all()->where('active',1);
        return View('admin.books.create',['categories'=>$categories]);
    }

    /**
     * this method adds a book in the database and redirects the
     * admin to the books view , all the validation is
     * done in the BookStoreRequest
     * @param BookStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postBook(BookStoreRequest $request){
        Book::add($request);
        return redirect('admin/books');
    }

    /**
     * this method deletes a book from the database
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteBook($id){
        $book = Book::findOrFail($id);
        /*
         * when deleting a book delete all of its comments , likes
         * and orders
         * $book->comments->delete();
         *   Comment::where('book_id',$id)->delete();
         */
        DB::table('comments')
            ->where('book_id', $id)
            ->delete();
        DB::table('likes')
            ->where('book_id', $id)
            ->delete();
        DB::table('orders')
            ->where('book_id', $id)
            ->delete();
        $book->delete();
        return back();
    }

    /**
     * this method sends the user to the edit view of some book
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditBook($id){
        $book=Book::findOrFail($id);
        $categories = Category::all()->where('active',1);
        return view('admin.books.edit',['book'=>$book,'categories'=>$categories]);
    }

    /**
     * this method updates some book in the database and then
     * redirects the admin to the books view , all the validation is
     * done in the BookStoreRequest
     * @param $id
     * @param BookStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editBook($id , BookStoreRequest $request){
        $book = Book::findOrFail($id);
        // update the fields in the database
        if($request['quantity'] > 0) {
            $book->update([
                'name' => $request['name'],
                'description' => $request['description'],
                'quantity' => $request['quantity'],
                'category_id' => $request['category'],
                'price' => $request['price'],
            ]);
        }else{
            $book->update([
                'name' => $request['name'],
                'description' => $request['description'],
                'quantity' => $request['quantity'],
                'category_id' => $request['category'],
                'price' => $request['price'],
                'active' => 0,
            ]);
        }
        return redirect('admin/books');
    }
    public function active(){
        $request = Request()->all();
        $isActive = $request['isActive'];
        $item = Book::findOrFail($request['id']);
        $item->active = ($isActive=="true")?1:0;
        $item->save();
        return $item;
    }
}
