<?php

namespace App\Http\Controllers\AdminsControllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequests\CategoryStoreRequest;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * this method sends the admin to the categories view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCategories(){
        $categories = Category::all();
        return view('admin.categories.index',['list'=>$categories]);
    }

    /**
     * this method adds a category in the database and then
     * redirects the admin to the category view , and the validation
     * is done in the CategoryStoreRequest class
     * @param CategoryStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCategory(CategoryStoreRequest $request){
        Category::add($request);
        return redirect('admin/categories');
    }

    /**
     * this method deletes some category from the database and
     * redirects the admin to the category view
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteCategory($id){
        $category = Category::findOrFail($id);
        // when deleting some category add its books to the super
        // category
        DB::table('books')
            ->where('category_id', $id)
            ->update(['category_id'=>1]);
        $category->delete();
        return back();
    }

    /**
     * this method sends the admin to the create view to add
     * categories
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addCategory(){
        return View('admin.categories.create');
    }

    /**
     * this method sends the admin to the edit view to edit some
     * category
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditCategory($id){
        $category=Category::findOrFail($id);
        return view('admin.categories.edit',['category'=>$category]);
    }

    /**
     * this method updates some category and then sends the admin
     * back to the categories view , and the validation is done
     * in the CategoryStoreRequest class
     * @param $id
     * @param CategoryStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editCategory($id , CategoryStoreRequest $request){
        $category = Category::findOrFail($id);
        $category->update([
            'name'=>$request->get('name')
        ]);
        return redirect('/admin/categories');
    }
    public function active(){
        $request = Request()->all();
        $isActive = $request['isActive'];
        $item = Category::findOrFail($request['id']);
        if($isActive=="true"){
            /**
             * if the category is activated then all of its books will be
             * activated
             */
            $item->active=1;
            $books = $item->books;
            foreach($books as $book){
                $book->active = 1;
                $book->save();
            }
        }else{
            $item->active=0;
            /*
             * if the category is deactivated then you can't add books
             * to it and all of its books will be deactivated
             */
            $books = $item->books;
            foreach($books as $book){
                $book->active = 0;
                $book->save();
            }
        }
        $item->save();
        return $item;
    }
}
