<?php

namespace App\Http\Controllers\AdminsControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequests\UserAddRequest;
use App\Http\Requests\UserRequests\UserUpdateRequest;
use App\User;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    /**
     * this method sends the admin to the users view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUsers(){
        $users = User::all();
        return view('admin.users.index',['list'=>$users]);
    }

    /**
     * the method sends the admin to the create view to add
     * some new user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addUser(){
        return View('admin.users.create');
    }

    /**
     * this method adds a new user in the database and then sends the
     * admin back to the users view , and all the validation is done
     * in the UserAddRequest class
     * @param UserAddRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUser(UserAddRequest $request){
        User::add($request);
        return redirect('admin/users');
    }

    /**
     * this method deletes a user from the database and then sends
     * him back to the users view
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteUser($id){
        $user = User::findOrFail($id);
        /*
         * when deleting a user delete its comments ,orders and likes
         */
        DB::table('comments')
            ->where('user_id', $id)
            ->delete();
        DB::table('likes')
            ->where('user_id', $id)
            ->delete();

        DB::table('orders')
            ->where('visitor_id', $id)
            ->update(['visitor_id'=>1]);
        $user->delete();
        return back();
    }

    /**
     * this method sends the admin to the edit page to edit some
     * user
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditUser($id){
        $user=User::findOrFail($id);
        return view('admin.users.edit',['user'=>$user]);
    }

    /**
     * this method updates some user and then sends the admin
     * back to the users view , and the validation is done
     * in the UserUpdateRequest class
     * @param $id
     * @param UserUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function editUser($id , UserUpdateRequest $request){
        $pass=$request['password'];
        $user = User::findOrFail($id);
        /*
         * if there is a password then update the field
         * if not then don't update it
         */
        if($pass){
            $user->update([
                'name'=>$request['name'],
                'email'=>$request['email'],
                'password'=>bcrypt($pass),
            ]);
        }else{
            $user->update([
                'name'=>$request['name'],
                'email'=>$request['email'],
            ]);
        }
        return redirect('admin/users');
    }
    public function active(){
        $request = Request()->all();
        $isActive = $request['isActive'];
        $item = User::findOrFail($request['id']);
        $item->active = ($isActive=="true")?1:0;
        $item->save();
        return $item;
    }
}
