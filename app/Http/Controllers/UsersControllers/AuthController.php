<?php

namespace App\Http\Controllers\UsersControllers;


use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequests\UserLoginRequest;
use App\Http\Requests\UserRequests\UserRegisterRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    /**
     * this method sends the user to the login page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin(){
        return View('auth.login');
    }

    /**
     * this method attempts to login the user , if successed he is
     * redirected to the home page and if didn't succeed sends him
     * back to the login page , all the validation is done in the
     * UserLoginRequest class
     * @param UserLoginRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function PostLogin(UserLoginRequest $request){
        $user = User::all()->where('email',request('email'))->first();
        if($user && !$user->active){
            Session::flash('error','account deactivated');
            return redirect('/login')->withInput();
        }
        if(Auth::attempt($request->only('email', 'password'))){
            if(!Auth::user()->active){
                Session::flash('error','wrong email or password');
                // session(['key'=>'value']) this session will live the whole user's session
                return redirect('/login')->withInput();
            }
            Auth::user()->loadLikes();
            return redirect('/user/home');
        }
        Session::flash('error','wrong email or password');
        return redirect('/login')->withInput();
    }

    /**
     * this method sends the user to the registering view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRegister(){
        return View('auth.register');
    }

    /**
     * this method registeres a user in the database and then sends
     * him the home page , all the validation is done the
     * UserRegisterRequest class
     * @param UserRegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postRegister(UserRegisterRequest $request){
        User::add($request);
        return redirect('/user/login');
    }

}
