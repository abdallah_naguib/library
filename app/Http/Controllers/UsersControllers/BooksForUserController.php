<?php

namespace App\Http\Controllers\UsersControllers;

<<<<<<< HEAD

=======
>>>>>>> 226a459ce6416b0676f2868e4c2b79241d85233b
use App\Book;
use App\Events\BookFinished;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookRequests\BookOrderRequest;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BooksForUserController extends Controller
{
    /**
     * this method sends the user to show view to see more info
     * on the book and orders
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id){
        $book = Book::findOrFail($id);
        return view('books.show',['book'=>$book]);
    }

    /**
     * this method is called when the user attempts to order
     * some book , all the validation is done in the
     * BookOrderRequest class
     * @param $id
     * @param BookOrderRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function order($id, Request $request){
        $this->validate($request,['quantity'=>'required']);
        $book = Book::findOrFail($id);
        $quantity = request('quantity');
        $book->updateQuantity($book->quantity - $quantity);
        $order = new Order();
        $order->visitor_id = Auth::user()->id;
        $order->book_id = $id;
        $order->quantity = $quantity;
        $order->save();
        return redirect('user/home');
    }
}
