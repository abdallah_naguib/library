<?php

namespace App\Http\Controllers\UsersControllers;

use App\Book;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequests\CommentRequest;

use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    /**
     * this method adds a comment to the database and it's called
     * when a user adds a comment , then he's redirected to the home
     * page , all the validation is done in the CommentRequest class
     * @param $id
     * @param CommentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create($id , CommentRequest $request){
        $comment = new Comment();
        $comment->book_id=$id;
        $comment->user_id=Auth::user()->id;
        $comment->content=$request['content'];
        $comment->save();
        $book = Book::find($id);
        $book->comments_cnt++;
        $book->save();
        $userName = Auth::user()->name;
        $res = [
                'id' => $comment->id,
                'name'=>$userName ,
                'content'=>$request['content'],
                'image'=>"/uploads/users/".$comment->user->image,
                'likes'=>$book->likes->count(),
                'comments'=>$book->comments->count()];
        return response()->json($res);
    }
    public function update($id,CommentRequest $request){
        dump($request->all());
        $comment = Comment::find($id);
        $comment->update(['content'=>$request['content']]);
    }
    public function delete($id){
        $comment = Comment::find($id);
        $book = $comment->book;
        $comment->delete();
        $book->comments_cnt--;
        $book->save();
        $res = [
            'likes'=>$book->likes->count(),
            'comments'=>$book->comments->count(),
            'book_id'=>$book->id,
        ];
        return response()->json($res);
    }
    public function getAll($book_id){
        $book = Book::find($book_id);
        $comments = $book->comments;
        return response()->json($comments);
    }
}
