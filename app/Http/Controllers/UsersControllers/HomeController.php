<?php

namespace App\Http\Controllers\UsersControllers;

use App\Book;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * this method sends the user to the home page where he can
     * see books add comments and likes to them
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $books = Book::all()->where('active',1);
        return view('home',['books'=>$books]);
    }

    /**
     * this method sends the user to the orders page where he can
     * see all of his transactions
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function orders()
    {
        return view('user-orders');
    }
}
