<?php

namespace App\Http\Controllers\UsersControllers;

use App\Book;
use App\Http\Controllers\Controller;
use App\Like;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{
    /**
     * this method adds a like to the database
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create($id){
        $user=Auth::user();
        $like = $user->hasLiked($id);
        $book = Book::find($id);
        if($like){
            // user already liked this book
            $book->likes_cnt--;
            $book->save();
            $like->delete();
            $res = [
                'likes'=>$book->likes->count(),
                'comments'=>$book->comments->count(),
                'liked'=>__('msg.like')];
            return response()->json($res);
        }
        $like = new Like();
        $like->book_id=$id;
        $like->user_id=Auth::user()->id;
        $like->save();
        $book->likes_cnt++;
        $book->save();
        $res = [
            'likes'=>$book->likes->count(),
            'comments'=>$book->comments->count(),
            'liked'=>__('msg.unlike')];
        return response()->json($res);
    }
}
