<?php

namespace App\Http\Middleware;

use App\Admin;
use App\User;
use Closure;

class APIkey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->apikey == '') {
            return response('Require access key');
        } else {
            $users = User::where('access_key', $request->apikey)->count();
            $admins = Admin::where('access_key', $request->apikey)->count();
            if ($users == 0 && $admins == 0) {
                return response("Invalid access key");
            } else {
                return $next($request);
            }
        }
    }
}
