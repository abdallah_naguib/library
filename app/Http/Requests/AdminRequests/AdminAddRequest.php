<?php

namespace App\Http\Requests\AdminRequests;

use Illuminate\Foundation\Http\FormRequest;

class AdminAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * to create an admin he must has a name , email and a password
         * and the email should be a valid one and needs to be unique
         * in admins table and the password needs to be at least
         * 8 characters
         */
        return [
            'name' => 'required',
            'email' => 'required|email|unique:admins,email',
            'password' => "required|min:3",
        ];
    }
}
