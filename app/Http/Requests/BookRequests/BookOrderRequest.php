<?php

namespace App\Http\Requests\BookRequests;

use App\Book;
use Illuminate\Foundation\Http\FormRequest;

class BookOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * to make an order request the user needs to add the quantity
         * and it must be greater than 0 , and less than or equal
         * the available quantity
         */
        $id = $this->request->get('book_id');
        if(!($id)){
            //then it is coming from task page by admin
            $id = $this->request->get('books_selector');
        }
        $book = Book::findOrFail($id);
        $available = $book->quantity;
        return [
            // lte means less than or equal
           // 'quantity' => 'required|lte:'.$available.'|gt:0'
            'quantity' => 'required'
        ];
    }


    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
   /* public function messages()
    {
        return [
            'quantity.required'=>'enter the quantity ,man',
        ];
    }*/
}
