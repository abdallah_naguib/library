<?php

namespace App\Http\Requests\BookRequests;

use Illuminate\Foundation\Http\FormRequest;


class BookStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * to store a book you need to give it a name , description ,
         * price and quantity
         */
        return [
            'name' => 'required',
            'description' => "required",
            'price' => "required",
            'quantity' => "required",
        ];
    }
}
