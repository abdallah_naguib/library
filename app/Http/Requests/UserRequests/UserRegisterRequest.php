<?php

namespace App\Http\Requests\UserRequests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * to create a user he must has a name , email and a password
         * and the email should be a valid one and needs to be unique
         * in users table and the password needs to be at least
         * 8 characters , the only difference between these rules
         * and the ones in the UserAddRequest is that the user needs
         * to add the password twice to confirm it
         */
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => "required|confirmed|min:3",
        ];
    }
}
