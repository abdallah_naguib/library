<?php

namespace App\Http\Requests\UserRequests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * to update a user/admin we need a name , email (valid email),
         * and a password which can be null in case the admin doesn't
         * wanna edit , but in case he provides a password it needs
         * to be at least 8 characters
         */
        return [
            'name' => 'required',
            'email' => 'required|email',
            'password' => "nullable|min:3",
        ];
    }
}
