<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class BooksResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     *
     */
    public function toArray($request)
    {
        $user = User::find(BooksResource::$userId);
        if($user){
            if($user->hasLiked($this->id)){
                $liked = true;
            }else{
                $liked=false;
            }
        }else{
            $liked = false;
        }
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price,
            'image' => $this->image,
            'quantity' => $this->quantity,
            'likes' => sizeof($this->likes),
            'active' => $this->active,
            'liked' => $liked,
            'category' => $this->category->name,
            'admin' => new AdminsResource($this->admin),
            'comments' => CommentsResource::collection($this->comments),
        ];
    }
}
