<?php

namespace App\Listeners;

use App\Events\BookFinished;
use App\Mail\BookIsFinished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendBookIsOutEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookFinished  $event
     * @return void
     */
    public function handle(BookFinished $event)
    {
        Mail::to($event->book->admin->email)->send(
            new BookIsFinished($event->book)
        );
    }
}
