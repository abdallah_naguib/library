<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    private $postsLiked = [];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function likes(){
        return $this->hasMany(Like::class);
    }

    /**
     * this method checks if this user has liked some book and if he
     * did it returns that like , but if he didn't it returns null
     * @param $book_id
     * @return null
     */
    public function hasLiked($book_id){
        $likes = $this->likes;
        foreach($likes as $like){
            if($like->book_id == $book_id){
                return $like;
            }
        }
        return null;
    }
    public function loadLikes(){
        $likes = $this->likes;
        foreach($likes as $like){
            $this->postsLiked[$like->book_id] = 1;
        }
    }
    /**
     * this method adds some user to the database
     * @param Request $request
     */
    public static function add(Request $request){
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move("uploads/users/",$filename);
            $user->image = $filename;
        }
        $user->access_key = Str::random(50);
        $user->save();
    }
}
