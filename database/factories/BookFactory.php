<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Admin;
use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    $categories=\App\Category::pluck('id')->toArray();
    $admins = Admin::pluck('id')->toArray();
    return [
        'category_id'=>$categories[rand(0,sizeof($categories)-1)],
        'admin_id'=>$admins[rand(0,sizeof($admins)-1)],
        'name' => $faker->sentence(4),
        'description' => $faker->sentence(4),
        'quantity'=>rand(100,1000),
        'price'=>rand(1000,5000),
    ];
});
