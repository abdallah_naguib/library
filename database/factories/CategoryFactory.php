<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Admin;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    $admins = Admin::pluck('id')->toArray();
    return [
        'admin_id'=>$admins[rand(0,sizeof($admins)-1)],
        'name'=>$faker->name,
    ];
});
