<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    $books=\App\Book::pluck('id')->toArray();
    $users=\App\User::pluck('id')->toArray();
    return [
        'book_id'=>$books[rand(0,sizeof($books) -1)],
        'user_id'=>$users[rand(0,sizeof($users) -1)],
        'content'=>$faker->sentence(4),
    ];
});
