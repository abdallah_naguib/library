<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\Like::class, function (Faker $faker) {
    $books=\App\Book::pluck('id')->toArray();
    $users=\App\User::pluck('id')->toArray();
    return [
        'book_id'=>$books[rand(0,sizeof($books) -1)],
        'user_id'=>$users[rand(0,sizeof($users) -1)],
    ];
});
