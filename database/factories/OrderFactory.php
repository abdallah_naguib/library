<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Order::class, function (Faker $faker) {
    $users = \App\User::pluck('id')->toArray();
    $books = \App\Book::pluck('id')->toArray();
    return [
        'visitor_id' => $users[rand(0,sizeof($users)-1)],
        'book_id' => $books[rand(0,sizeof($books)-1)],
        'quantity' => rand(1,100),
    ];
});
