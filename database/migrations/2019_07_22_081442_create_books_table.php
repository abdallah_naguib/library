<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->decimal('price');
            $table->integer('quantity');
            $table->string('image')->default('book.jpeg');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('admin_id');
            $table->boolean('active')->default(1);
            $table->unsignedInteger('likes_cnt')->default(0);
            $table->unsignedInteger('comments_cnt')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
