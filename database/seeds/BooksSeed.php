<?php

use Illuminate\Database\Seeder;
use App\Book;

class BooksSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * $table->bigIncrements('id');
     */

    public function run()
    {
        factory(App\Book::class, 50)->create();
        /*$books = Book::all();
        foreach ($books as $book){
            $book->comments_cnt = $book->comments->count();
            $book->likes_cnt = $book->likes->count();
            $book->save();
        }*/
    }
}
