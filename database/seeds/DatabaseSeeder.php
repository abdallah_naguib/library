<?php

use App\Admin;
use Illuminate\Database\Seeder;
use App\Book;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
     //
       // $this->call(CommentsSeeder::class);
        $this->call(AdminsSeed::class);
        $this->call(UsersSeeder::class);
        $this->call(CategoriesSeed::class);
        $this->call(BooksSeed::class);
        $this->call(LikesSeeder::class);
        $this->call(CommentsSeeder::class);
        $this->call(OrdersSeeder::class);

        foreach(Book::all() as $book){
            $book->active=1;
            $book->save();
        }

    }
}
