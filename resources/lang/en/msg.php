<?php
/**
 * Created by PhpStorm.
 * User: abdo
 * Date: 8/1/19
 * Time: 3:31 PM
 */
return ["content"=>"home",
    'active'=>'active',
    'category_deactivated'=>'category is deactivated',
    "orders"=>'orders',
    "name"=>'name',
    "description"=>'description',
    "price"=>'price',
    'quantity'=>'quantity',
    'ordered_at'=>'ordered_at',
    'my_orders'=>'my orders',
    'load_more'=>'load more posts',
    'like'=>'like',
    'likes'=>'likes',
    'unlike'=>'unlike',
    'comments'=>'comments',
    'order'=>'order',
    'out_of_stock'=>'out of stock',
    'submit'=>'add',
    'write_comment'=>'write a comment',
    'enter_quantity'=>'Enter quantity',
    'dashboard'=>'dashboard',
    'visitors'=>'Visitors',
    'admins'=>'Admins',
    'categories'=>'Categories',
    'books'=>'Books',
    'add_user'=>'Add user',
    'add_admin'=>'Add admin',
    'add_book'=>'Add book',
    'add_category'=>'Add category',
    'delete'=>'delete',
    'edit'=>'edit',
    'email'=>'Email',
    'password'=>'Password',
    'created_at'=>'created_at',
    'updated_at'=>'updated_at',
    'category'=>'category',
    'enter_name'=>'Enter name',
    'enter_email'=>'Enter email',
    'enter_password'=>'Enter password',
    'upload_photo'=>'Upload photo',
    'enter_desc'=>'Enter description',
    'enter_price'=>'Enter price',
    'update_admin'=>'update admin',
    'update_book'=>'update book',
    'update_user'=>'update user',
    'update_category'=>'update category',
    'update'=>'update',
    'task'=>'task',
    'log-out'=>'Logout',
    'Admin_login'=>'Admin login',
    'login'=>'Login',
    'register'=>'Register',
    'confirm_password'=>'Confirm password'
];