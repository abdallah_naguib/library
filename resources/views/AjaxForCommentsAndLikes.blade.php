<script>
    function addComment(id){
        console.log(id);
        $.ajaxSetup({
            beforeSend: function(xhr, type) {
                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                }
            },
        });
        var token="{{csrf_token()}}";
        var content = $('#comment-input-'+id).val();
        console.log(content);
        $('#comment-input-'+id).val('');
        var URL = "{{url('user/books')}}"+"/"+id+"/comment";
        $.ajax({
            type:'POST',
            url:URL,
            data: {
                '_token': token,
                'content':content,
            },
            success:function(data) {
                const img = data['image'];
                var likesText = "{{__('msg.likes')}}";
                var commentsText = "{{__('msg.comments')}}";
                $('#likes-comments-counter-'+id).html(data['likes']+" "+likesText
                    +" - "
                    +data['comments']+" "+commentsText);

                $('#comments-div-'+id).append('<div class="box-comment">\n' +
                    '    <!-- User image -->\n' +
                    '    <img class="img-circle img-sm" src='+img+' alt="User Image">\n' +
                    '\n' +
                    '    <div class="comment-text">\n' +
                    '                      <span class="username">\n' +
                    data['name']+ '\n' +
                    '                      </span>\n' +
                    data['content']+'\n' +
                    '    </div>\n' +
                    '\n' +
                    '</div>');
            }
        });
    }
    function addLike(id){
        console.log(id);
        $.ajaxSetup({
            beforeSend: function(xhr, type) {
                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                }
            },
        });
        var token="{{csrf_token()}}";
        var URL = "{{url('user/books')}}"+"/"+id+"/like";
        $.ajax({
            type:'POST',
            url:URL,
            data: {
                '_token': token,
            },
            success:function(data) {
                var likesText = "{{__('msg.likes')}}";
                var commentsText = "{{__('msg.comments')}}";
                $('#like-'+id).html('<i class="fa fa-thumbs-o-up"></i>'+
                    data['liked']);
                $('#likes-comments-counter-'+id).html(data['likes']+" "+likesText
                    +" - "
                    +data['comments']+" "+commentsText);
            }
        });
    }
</script>