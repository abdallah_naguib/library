@extends('admin.home')
@section('content')
<div class="box box-primary">
    <h1>{{__('msg.add_book')}}</h1>
    <form action="/admin/books/create" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="box-body">
            <div class="form-group col-md-4">
                <label >{{__('msg.name')}}</label>
                <input name="name"
                       type="text" class="form-control"
                       placeholder="{{__('msg.enter_name')}}" value="{{old('name')}}">
            </div>
            <div class="form-group col-md-4">
                <label>{{__('msg.description')}}</label>
                <input name="description"
                       type="text" class="form-control"
                       placeholder="{{__('msg.enter_desc')}}"
                       value="{{old('description')}}"
                >
            </div>
            <div class="form-group col-md-4">
                <label>{{__('msg.price')}}</label>
                <input name="price"
                       type="number" class="form-control"
                       placeholder="{{__('msg.enter_price')}}"
                        value="{{old('price')}}">
            </div>

            <div class="form-group col-md-4">
                @if(@$errors)
                    @foreach($errors->get('name') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>
            <div class="form-group col-md-4">
                @if(@$errors)
                    @foreach($errors->get('description') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>
            <div class="form-group col-md-4">
                @if(@$errors)
                    @foreach($errors->get('price') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>
            <div class="form-group col-md-4">
                <label>{{__('msg.quantity')}}</label>
                <input name="quantity"
                       type="number" class="form-control"
                       placeholder="{{__('msg.enter_quantity')}}"
                        value="{{old('quantity')}}">
            </div>
            <div class="form-group col-md-4">
                <label>{{__('msg.category')}}</label>
                <select class="select-css" name="category">

                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label>{{__('msg.upload_photo')}}</label>
                <input type="file" name="image" id="exampleInputFile">
            </div>
            <div class="form-group col-md-4">
                @if(@$errors)
                    @foreach($errors->get('quantity') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{__('msg.submit')}}</button>
        </div>
    </form>
</div>
@endsection