@extends('admin.home')
@section('content')
    <div class="box box-primary">
        <h1>{{__('msg.update_book')}}</h1>
        <form action="/admin/books/{{$book->id}}" method="post" enctype="multipart/form-data">
            {{method_field('patch')}}
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group col-md-4">
                    <label >{{__('msg.name')}}</label>
                    <input name="name"
                           type="text" class="form-control"
                           value="{{$book->name}}">
                </div>
                <div class="form-group col-md-4">
                    <label>{{__('msg.description')}}</label>
                    <input name="description"
                           type="text" class="form-control"
                           value="{{$book->description}}"
                    >
                </div>
                <div class="form-group col-md-4">
                    <label>{{__('msg.price')}}</label>
                    <input name="price"
                           type="number" class="form-control"
                           value="{{$book->price}}">
                </div><div class="form-group col-md-4">
                    @if(@$errors)
                        @foreach($errors->get('name') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                        @endforeach
                    @endif
                </div>
                <div class="form-group col-md-4">
                    @if(@$errors)
                        @foreach($errors->get('description') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                        @endforeach
                    @endif
                </div>
                <div class="form-group col-md-4">
                    @if(@$errors)
                        @foreach($errors->get('price') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                        @endforeach
                    @endif
                </div>
                <div class="form-group col-md-4">
                    <label>{{__('msg.quantity')}}</label>
                    <input name="quantity"
                           type="number" class="form-control"
                           value="{{$book->quantity}}">
                </div>
                <div class="form-group col-md-4">
                    <label>{{__('msg.category')}}</label>
                    <select class="select-css" name="category">
                        @foreach($categories as $category)
                            @if($book->category_id == $category->id)
                                <option value="{{$category->id}}" selected>{{$category->name}}</option>
                            @else
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    @if(@$errors)
                        @foreach($errors->get('quantity') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                        @endforeach
                    @endif
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{__('msg.update')}}</button>
            </div>
        </form>
    </div>
@endsection