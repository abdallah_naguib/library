@extends('admin.home')
@section('css')
    <style>
        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endsection
@section('content')

    <section class="content">
        <div class="box box-primary">
            <form action="/admin/books/create" method="get" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">{{__('msg.add_book')}}</button>
                </div>
            </form>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{__('msg.books')}}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{{__('msg.name')}}</th>
                        <th>{{__('msg.description')}}</th>
                        <th>{{__('msg.category')}}</th>
                        <th>{{__('msg.price')}}</th>
                        <th>{{__('msg.quantity')}}</th>
                        <th>{{__('msg.created_at')}}</th>
                        <th>{{__('msg.updated_at')}}</th>
                        <th></th>
                        <th></th>
                        <th>{{__('msg.active')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($list as $item)
                        <tr>
                            <th>{{$item->name}}</th>
                            <th>{{$item->description}}</th>
                            <th>{{$item->category->name}}</th>
                            <th>{{$item->price}}</th>
                            <th>{{$item->quantity}}</th>
                            <th>{{$item->created_at}}</th>
                            <th>{{$item->updated_at}}</th>
                            <th>
                                <form method="post" action="/admin/books/{{$item->id}}">
                                    {{method_field('delete')}}
                                    {{csrf_field()}}
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">{{__('msg.delete')}}</button>
                                    </div>
                                </form>
                            </th>
                            <th>
                                <form action="/admin/books/{{$item->id}}/edit" method="get">
                                    {{csrf_field()}}
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">{{__('msg.edit')}}</button>
                                    </div>
                                </form>
                            </th>
                            <th>
                                @if($item->quantity > 0 && $item->category->active)
                                    <label class="switch">
                                        <input type="checkbox"
                                               id="isActive{{$item->id}}"
                                               name="isActive"
                                                {{$item->active?"checked":""}}
                                                onclick="makeActive({{$item->id}})">
                                        <span class="slider round"></span>
                                    </label>
                                @elseif($item->quantity == 0)
                                    {{__('msg.out_of_stock')}}
                                @elseif(!($item->category->active))
                                    {{__('msg.category_deactivated')}}
                                @endif
                            </th>
                        </tr>
                    @endforeach
                    <tfoot>
                    <tr>
                        <th>{{__('msg.name')}}</th>
                        <th>{{__('msg.description')}}</th>
                        <th>{{__('msg.category')}}</th>
                        <th>{{__('msg.price')}}</th>
                        <th>{{__('msg.quantity')}}</th>
                        <th>{{__('msg.created_at')}}</th>
                        <th>{{__('msg.updated_at')}}</th>
                        <th></th>
                        <th></th>
                        <th>{{__('msg.active')}}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
@endsection
@section('script')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- jQuery 3 -->
    <script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('backend/bower_components/fastclick/lib/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('backend/dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
        function makeActive(id) {
            if(id == -1){
                alert("add quantity to the book to make it active");
            }else {
                var isActive = $('#isActive' + id).is(":checked");
                $.ajaxSetup({
                    beforeSend: function (xhr, type) {
                        if (!type.crossDomain) {
                            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                        }
                    },
                });
                var token = "{{csrf_token()}}";
                $('#comment-input-' + id).val('');
                var URL = "{{url('admin/books/active')}}";
                $.ajax({
                    type: 'POST',
                    url: URL,
                    data: {
                        '_token': token,
                        'isActive': isActive,
                        'id': id,
                    },
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        }
    </script>
@endsection