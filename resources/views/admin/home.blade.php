@extends('layouts.layout')

@push('sidebar')
    <li><a href="/admin">{{__('msg.dashboard')}}</a></li>
    <li><a href="/admin/users">{{__('msg.visitors')}}</a></li>
    <li><a href="/admin/admins">{{__('msg.admins')}}</a></li>
    <li><a href="/admin/categories">{{__('msg.categories')}}</a></li>
    <li><a href="/admin/books">{{__('msg.books')}}</a></li>
    <li><a href="/admin/task">{{__('msg.task')}}</a></li>
@endpush
