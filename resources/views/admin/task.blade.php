@extends('admin.home')
@section('content')
    <form action="/admin/order" method="post">
        {{csrf_field()}}
        <select id="admins-selector" name="admins_selector">
            <option value="-1" selected>{{__('msg.admins')}}</option>
        </select>
        <select id="categories-selector" name="categories_selector">
            <option value="-1" selected>{{__('msg.categories')}}</option>
        </select>
        <select id="books-selector" name="books_selector">
            <option value="-1" selected>{{__('msg.books')}}</option>
        </select>
        <select id="users-selector" name="visitors_selector">
            <option value="-1" selected>{{__('msg.visitors')}}</option>
        </select>
        <div id="available"></div>
        <input type="number" name="quantity"
               value="{{old('quantity')}}"
               placeholder="@lang('msg.enter_quantity')">
        @if($errors)
            @foreach($errors->get('quantity') as $message)
                <span class='help-inline text-danger'>{{ $message }}</span>
            @endforeach
        @endif
        <input type="submit" value="@lang('msg.order')" id="orderByAdmin">
    </form>
@endsection
@section('script')
    <script>
    $(document).ready(function(){
        $.ajaxSetup({
            beforeSend: function(xhr, type) {
                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                }
            },
        });
        $('#orderByAdmin').hide();
        let token="{{csrf_token()}}";
        let access = "{{Auth::user()->access_key}}";
        let URL = "{{url('api/admins')}}"+"/access_key="+access;
        let categories = "{{__('msg.categories')}}";
        let books = "{{__('msg.books')}}";
        $.ajax({
            type:'GET',
            url:URL,
            data: {
                '_token': token,
            },
            success:function(data) {
                for(let i=0; i<data['data'].length ; i++) {
                    let admin=data['data'][i];
                    $('#admins-selector').append('<option value="' +
                        admin['id']+'"> ' + admin['name']+
                        '</option>');
                }
            }
        });
        access = "{{Auth::user()->access_key}}";
        URL = "{{url('api/users')}}"+"/access_key="+access;
        $.ajax({
            type:'GET',
            url:URL,
            data: {
                '_token': token,
            },
            success:function(data) {
                for(let i=0; i<data['data'].length ; i++) {
                    let user=data['data'][i];
                    $('#users-selector').append('<option value="' +
                        user['id']+'"> ' + user['name']+
                        '</option>');
                }
            }
        });
        $('#admins-selector').change(function () {
            let id=$('#admins-selector').val();
            let access = "{{Auth::user()->access_key}}";
            let URL = "{{url('api')}}"+"/"+id+"/categories/access_key="+access;
            $.ajax({
                type:'GET',
                url:URL,
                data: {
                    '_token': token,
                },
                success:function(data) {
                    console.log(data);
                    $('#available').html('');
                    $('#orderByAdmin').hide();
                    $('#categories-selector').html('<option value="-1" selected>'+categories+'</option>');
                    $('#books-selector').html('<option value="-1" selected>'+books+'</option>');
                    for(let i=0; i<data['data'].length ; i++) {
                        let category=data['data'][i];
                        $('#categories-selector').append('<option value="' +
                            category['id']+'"> ' + category['name']+
                            '</option>');
                    }
                }
            });
        });
        let quantities=[];
        $('#categories-selector').change(function () {
            let catId=$('#categories-selector').val();
            let access = "{{Auth::user()->access_key}}";
            let URL = "{{url('api')}}"+"/"+catId+"/books/access_key="+access;
            console.log(URL);
            $.ajax({
                type:'GET',
                url:URL,
                data: {
                    '_token': token,
                },
                success:function(data) {
                    $('#available').html('');
                    $('#orderByAdmin').hide();
                    $('#books-selector').html('<option value="-1" selected>'+books+'</option>');
                    for(let i=0; i<data['data'].length ; i++) {
                        let book=data['data'][i];
                        quantities[book['id']]=book['quantity'];
                        $('#books-selector').append('<option value="' +
                            book['id']+'"> ' + book['name']+
                            '</option>');
                    }
                }
            });
        });
        $('#books-selector').change(function () {
           let bookId = $('#books-selector').val();
           if(bookId != -1){
               if($('#users-selector').val() != -1) {
                   $('#orderByAdmin').show();
               }
               $('#available').html(quantities[bookId]+' books available.');
           }else{
               $('#orderByAdmin').hide();
               $('#available').html('');
           }
        });
        $('#users-selector').change(function () {
            let bookId = $('#books-selector').val();
            if(bookId != -1 && $('#users-selector').val() != -1) {
                $('#orderByAdmin').show();
            }else{
                $('#orderByAdmin').hide();
            }
        });
    })
    </script>
@endsection