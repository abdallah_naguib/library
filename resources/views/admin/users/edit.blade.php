@extends('admin.home')
@section('content')
    <div class="box box-primary">
        <h1>{{__('msg.update_user')}}</h1>
        <form method="post" action="/admin/users/{{$user->id}}" enctype="multipart/form-data">
            {{method_field('patch')}}
            {{csrf_field()}}
            <div class="box-body">
                <div class="form-group col-md-4">
                    <label>{{__('msg.name')}}</label>
                    <input name="name"
                           type="text" class="form-control"
                           value="{{$user->name}}">
                </div>
                <div class="form-group col-md-4">
                    <label>{{__('msg.email')}}</label>
                    <input name="email"
                           type="text" class="form-control"
                           value="{{$user->email}}"
                    >
                </div>
                <div class="form-group col-md-4">
                    <label>{{__('msg.password')}}</label>
                    <input name="password"
                           type="password" class="form-control"
                           placeholder="{{__('msg.enter_password')}}">
                </div>
                <div class="col-md-4">
                    @if(@$errors)
                        @foreach($errors->get('name') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-4">
                    @if(@$errors)
                        @foreach($errors->get('email') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-4">
                    @if(@$errors)
                        @foreach($errors->get('password') as $message)
                            <span class='help-inline text-danger'>{{ $message }}</span>
                        @endforeach
                    @endif
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{__('msg.update')}}</button>
            </div>
        </form>
    </div>
@endsection