<html>
<head><title>login</title></head>
<body>
@include('css')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- general form elements -->
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{__('msg.Admin_login')}}</h3>
        <select id="lang-select">
            <option value="en" selected>en</option>
            <option value="ar">ar</option>
        </select>
    </div>
    @if(Session::has('error'))
        <div class="alert alert-error">
            {{ Session::get('error') }}
        </div>
    @endif
    <!-- /.box-header -->
    <!-- form start -->
    <form method="POST" action="{{url('admin/logn')}}">
        {{csrf_field()}}
        <div class="box-body">
            <div class="form-group">
                <label>{{__('msg.email')}}</label>
                <input name="email" type="email" class="form-control" id="exampleInputEmail1"
                       placeholder="{{__('msg.enter_email')}}" value="{{old('email')}}">
                @if(@$errors)
                    @foreach($errors->get('email') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>
            <div class="form-group">
                <label>{{__('msg.password')}}</label>
                <input type="password" name="password" class="form-control"
                       id="exampleInputPassword1" placeholder="{{__('msg.enter_password')}}">
                @if(@$errors)
                    @foreach($errors->get('password') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">{{__('msg.login')}}</button>
        </div>

    </form>
</div>
<!-- /.box -->

@include('lang-script')
</body>
</html>