<html>
<head><title>register</title></head>
<body>
@include('css')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{__('msg.register')}}</h3>
        <select id="lang-select">
            <option value="en" selected>en</option>
            <option value="ar">ar</option>
        </select>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form method="post" action="/user/regster" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="box-body">
            <div class="form-group">
                <label>{{__('msg.name')}}</label>
                <input name="name" class="form-control" id="exampleInputEmail1"
                       placeholder="{{__('msg.enter_name')}}" value="{{old('name')}}">
                @if(@$errors)
                    @foreach($errors->get('name') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>
            <div class="form-group">
                <label>{{__('msg.email')}}</label>
                <input name="email" type="email" class="form-control"
                       id="exampleInputEmail1" placeholder="{{__('msg.enter_email')}}"
                        value="{{old('email')}}">
                @if(@$errors)
                    @foreach($errors->get('email') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>
            <div class="form-group">
                <label>{{__('msg.password')}}</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="{{__('msg.enter_password')}}">
                @if(@$errors)
                    @foreach($errors->get('password') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>
            <div class="form-group">
                <label>{{__('msg.confirm_password')}}</label>
                <input type="password" name="password_confirmation" class="form-control" id="exampleInputPassword1" placeholder="{{__('msg.enter_password')}}">
                @if(@$errors)
                    @foreach($errors->get('password') as $message)
                        <span class='help-inline text-danger'>{{ $message }}</span>
                    @endforeach
                @endif
            </div>
            <div class="form-group">
                <label>{{__('msg.upload_photo')}}</label>
                <input type="file" name="image" id="exampleInputFile">
            </div>
        </div>
        <div class="box-footer">
            <a href="/login">{{__('msg.login')}}</a><br><br>
            <button type="submit" class="btn btn-primary">{{__('msg.register')}}</button>
        </div>
    </form>
</div>
<!-- /.box -->
@include('lang-script')
</body>
</html>