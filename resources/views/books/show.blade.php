@extends('layouts.layout')

@section('content')
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('backend/dist/css/skins/_all-skins.min.css')}}">
    <div class="row">
        <div class="col-md-12">
            <!-- Box Comment -->
            <div class="box box-widget">
                <div class="box-header with-border">
                    <div class="user-block">
                        <img class="img-circle" src="{{asset('uploads/users/'.\App\Admin::find($book->admin_id)->image)}}" alt="User Image">
                        <span class="username"><a href="#">{{\App\Admin::find($book->admin_id)->name}}</a></span>
                    </div>
                    <!-- /.user-block -->
                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    <!-- /.box-tools -->

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h1>{{$book->name}}</h1>
                    <h1>{{$book->category->name}}</h1>
                    <h3>{{$book->description}}</h3>
                    <img class="img-responsive pad" src="{{asset("uploads/books/".$book->image)}}" alt="Photo">
                    <button type="submit" class="btn btn-default btn-xs"
                    id="like-{{$book->id}}"
                    onclick="addLike({{$book->id}})">
                        <i class="fa fa-thumbs-o-up"></i>
                        @if(\Illuminate\Support\Facades\Auth::user()->hasLiked($book->id))
                            {{__('msg.unlike')}}
                        @else
                            {{__('msg.like')}}
                        @endif
                    </button>
                    <span class="pull-right text-muted"
                          id="likes-comments-counter-{{$book->id}}">
                        {{$book->likes->count()}} {{__('msg.likes')}} - {{$book->comments->count()}} {{__('msg.comments')}}
                    </span>
                    <div>
                        <h3>{{__('msg.price')}} : {{$book->price}} $</h3>
                        <h3>{{__('msg.quantity')}} : {{$book->quantity}}</h3>
                    </div>
                    <form method="post" action="/user/books/{{$book->id}}/order">
                        {{csrf_field()}}
                        <input name="quantity"
                               type="number" class="form-control"
                               value="{{old('quantity')}}"
                               placeholder="{{__('msg.enter_quantity')}}">
                        <input type="number" value="{{$book->id}}"
                               name="book_id" hidden>
                        <!-- this hidden is to know the available quantity and the reason
                          I am not putting the available quantity directly because some
                          other user might order some copies so the available quantity
                          will be updated -->
                        <input type="submit"value="{{__('msg.order')}}" >
                        @if(sizeof($errors))
                        @foreach($errors->get('quantity') as $message)
                                <span class='help-inline text-danger'>{{ $message }}</span>
                            @endforeach
                        @endif
                    </form>

                    <!-- /.box-body -->
                    <div class="box-footer box-comments"
                            id="comments-div-{{$book->id}}">
                        @each('_comment',$book->comments,'comment')
                    </div>
                    <!-- /.box-footer -->
                    <div class="box-footer">
                        <img class="img-responsive img-circle img-sm" src="{{asset("uploads/users/".Auth::user()->image)}}" alt="Alt Text">
                        <div class="img-push">
                            <input name="content"
                             id="comment-input-{{$book->id}}"
                                   type="text" class="form-control input-sm"
                                    placeholder="{{__('msg.write_comment')}}">
                            <button onclick="addComment({{$book->id}})" type="button">
                                    {{__('msg.submit')}}
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div></div>
@endsection
@push('sidebar')
    <li><a href="/user/home">{{__('msg.content')}}</a></li>
    <li><a href="/user/orders">{{__('msg.my_orders')}}</a></li>
@endpush
@section('script')
    @include('AjaxForCommentsAndLikes')
@endsection