@extends('layouts.layout')
@section('content')
    <center>
        <div onclick="get_page()" id="load-more-pages" style="color: rgba(32,186,198,0.99);
                                    cursor: pointer;">
            {{__('msg.load_more')}}
        </div>
    </center>
@endsection
@push('sidebar')
    <li><a href="/user/home">{{__('msg.content')}}</a></li>
    <li><a href="/user/orders">{{__('msg.my_orders')}}</a></li>
@endpush
@section('script')
    <script>
        $(document).ready(function () {
            get_page();
        });
        let x = -1;
        $(document).ready(function(){
            console.log("{{Auth::user()->id}}");
            get_page();
        });
        var x=-1;
        function get_page(){
            x++;
            $.ajaxSetup({
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    }
                },
            });
            var token="{{csrf_token()}}";
            var img = '/uploads/users/'+"{{Auth::user()->image}}";
            var URL = "{{url('api/books')}}"+"/"+user+"/"+x;
            var access_key="{{Auth::user()->access_key}}";
            var userId="{{Auth::user()->id}}";
            var URL = "{{url('api/books')}}"+"/"+userId+"/"+x+"/access_key="+access_key;
            var likes = "{{__('msg.likes')}}";
            var commentsText = "{{__('msg.comments')}}";
            console.log(URL);
            $.ajax({
                type:'GET',
                url:URL,
                data: {
                    '_token': token,
                },
                success:function(data) {
                    console.log(data);
                    if (data.length === 0) {
                        $('#load-more-pages').hide();
                    }
                    for (let i = 0; i < data.length; i++) {
                        var post = data[i];
                        if (!post['active']) continue;
                        let likeButtonText;
                        if (post["liked"]) {
                            likeButtonText = "{{__('msg.unlike')}}";
                        } else {
                            likeButtonText = "like";
                        }
                        let orderDiv;
                        if (post['quantity'] <= 0) {
                            orderDiv = 'out of stock';
                        } else {
                            orderDiv = '<form method="get" action="/user/books/' + post["id"] + '/show">\n' +
                                '                       <input type="submit" value="order">' +
                                '                    </form>\n';
                        }

                        $('#posts').append('<div class="row">\n' +
                            '    <div class="col-md-12" id="' + data['id'] + '">\n' +
                            '        \n');
                        likeButtonText = "{{__('msg.like')}}";
                    }
                    let orderDiv = '<form method="get" action="/user/books/' + post["id"] + '/show">\n' +
                        '                       <input type="submit" value="' + "{{__("msg.order")}}" + '">' +
                        '                    </form>\n';
                    let comments = '<div class="box-footer box-comments" id="comments-div-' + post["id"] + '">';
                    for (let j = 0; j < post["comments"].length; j++) {
                        comments += '<div class="box-comment">\n' +
                            '    <!-- User image -->\n' +
                            '    <img class="img-circle img-sm" src="/uploads/users/' + post["comments"][j]["image"] + '" alt="User Image">\n' +
                            '\n' +
                            '    <div class="comment-text">\n' +
                            '                      <span class="username">\n' +
                            post["comments"][j]["name"] +
                            '                      </span>\n' +
                            post["comments"][j]["content"] +
                            '    </div>\n' +
                            '\n' +
                            '</div>';
                    }
                    comments += '</div>\n' +
                        '                <!-- /.box-footer -->\n' +
                        '                <div class="box-footer">\n' +
                        '                    <img class="img-responsive img-circle img-sm" src="' + img + '" alt="Alt Text">\n' +
                        '                    <!-- .img-push is used to add margin to elements next to floating images -->\n' +
                        '                    <div class="img-push">\n' +
                        '                        <input name="content"\n' +
                        '                               id="comment-input-' + post["id"] + '"\n' +
                        '                               type="text" class="form-control input-sm"\n' +
                        '                               placeholder="' + "{{__('msg.write_comment')}}" + '">\n' +
                        '                        <button onclick="addComment(' + post["id"] + ')" type="button">\n' +
                        '                            ' + "{{__('msg.submit')}}" + '</button>\n' +
                        '\n' +
                        '                    </div>\n' +
                        '                </div>';
                    $('#posts').append('<br><br><div class="row">\n' +
                        '<div class="col-md-1"></div>' +
                        '    <div class="col-md-10">\n' +
                        '        <!-- Box Comment -->\n' +
                        '        <div class="box box-widget">\n' +
                        '            <div class="box-header with-border">\n' +
                        '                <div class="user-block">\n' +
                        '                    <img class="img-circle" src="/uploads/users/' + post['admin_img'] + '" alt="User Image">\n' +
                        '                    <span class="username"><a href="#">' + post["admin_name"] + '</a></span>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '                <h1>' + post["book_name"] + '</h1>\n' +
                        '                <img class="img-responsive pad" src="/uploads/books/' + post["book_img"] + '" alt="Photo">\n' +
                        '                <button type="submit"\n' +
                        '                        class="btn btn-default btn-xs"\n' +
                        '                        id="like-' + post["id"] + '"\n' +
                        '                        onclick="addLike(' + post["id"] + ')"><i class="fa fa-thumbs-o-up"></i>\n' +
                        likeButtonText +
                        '                </button>\n' +
                        '                <span class="pull-right text-muted"   ' +
                        '                      id="likes-comments-counter-' + post["id"] + '">' + post["likes_cnt"] + ' likes - ' + post["comments_cnt"] + ' comments</span>\n' +
                        orderDiv + '<a style="color: rgba(32,186,198,0.99);\n' +
                        '                                    cursor: pointer;" onclick="getComments(' + post['id'] + ')">get comments</a>' +
                        '                      id="likes-comments-counter-' + post["id"] + '">' + post["likes"] + ' ' + likes + ' - ' + post["comments"].length + ' ' + commentsText + '</span>\n' +
                        orderDiv + comments +
                        '            </div>\n' +
                        '            <!-- /.box -->\n' +
                        '        </div>\n' +
                        '        <!-- /.col -->\n' +
                        '    <div class="box-footer box-comments" id="comments-div-' + post["id"] + '">' +
                        '    </div>' +
                        '    </div>\n' +
                        '</div>');
                }
            });
        }

        function addComment(id){
            $.ajaxSetup({
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    }
                },
            });
            var token="{{csrf_token()}}";
            var content = $('#comment-input-'+id).val();
            $('#comment-input-'+id).val('');
            var URL = "{{url('user/books')}}"+"/"+id+"/comment";
            $.ajax({
                type:'POST',
                url:URL,
                data: {
                    '_token': token,
                    'content':content,
                },
                success:function(data) {
                    const img = data['image'];
                    $('#likes-comments-counter-'+id).html(data['likes']+" likes - "
                        +data['comments']+" comments");

                    $('#comments-div-'+id).append('<div class="box-comment" id="comment'+data['id']+'">\n' +
                        '    <!-- User image -->\n' +
                        '    <img class="img-circle img-sm" src='+img+' alt="User Image">\n' +
                        '\n' +
                        '    <div class="comment-text">\n' +
                        '                      <span class="username">\n' +
                        data['name']+ '\n' +
                        '                      </span>\n' +
                        data['content']+'\n' +
                        '    </div>\n' +
                        '\n <a style="cursor: pointer" onclick="editComment('+data['id']+',\''+data['content']+'\')">edit</a> '+
                        '<a style="cursor: pointer" onclick="deleteComment('+data['id']+')"> delete</a> '+
                        '</div>');
                }
            });
        }
        function addLike(id){
            $.ajaxSetup({
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    }
                },
            });
            var token="{{csrf_token()}}";
            var URL = "{{url('user/books')}}"+"/"+id+"/like";
            $.ajax({
                type:'POST',
                url:URL,
                data: {
                    '_token': token,
                },
                success:function(data) {
                    $('#like-'+id).html('<i class="fa fa-thumbs-o-up"></i>'+
                        data['liked']);
                    $('#likes-comments-counter-'+id).html(data['likes']+" likes - "
                        +data['comments']+" comments");
                }
            });
        }
        function getComments(bookId) {
            console.log(bookId);
            $.ajaxSetup({
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    }
                },
            });
            var token="{{csrf_token()}}";
            var URL = "{{url('api/comments')}}"+"/"+bookId;
            $.ajax({
                type:'get',
                url:URL,
                data: {
                    '_token': token,
                },
                success:function(data) {
                    let comments='<div class="box-footer box-comments" id="comments-div-'+post["id"]+'">';
                    let tail='';
                    for(let j=0 ; j<data.length ; j++){
                        let comment = data[j];
                        if(comment['user_id'] == user){
                            tail = '\n <a style="cursor: pointer" onclick="editComment('+comment['id']+',\''+comment['content']+'\')">edit</a> '+
                                '<a style="cursor: pointer" onclick="deleteComment('+comment['id']+')"> delete</a> ';
                        }
                        comments+='<div class="box-comment" id="comment'+comment['id']+'">\n' +
                            '    \n' +
                            '    <img class="img-circle img-sm" src="/uploads/users/'+comment["image"]+'" alt="User Image">\n' +
                            '\n' +
                            '    <div class="comment-text">\n' +
                            '                      <span class="username">\n' +
                            comment["name"] +
                            '                      </span>\n' +
                            comment["content"] +
                            '    </div>\n' +
                            tail+
                            '</div>';
                    }
                    comments+='</div>\n' +
                        '                \n' +
                        '                <div class="box-footer">\n' +
                        '                    <img class="img-responsive img-circle img-sm" src="'+img+'" alt="Alt Text">\n' +
                        '                    \n' +
                        '                    <div class="img-push">\n' +
                        '                        <input name="content"\n' +
                        '                               id="comment-input-'+post["id"]+'"\n' +
                        '                               type="text" class="form-control input-sm"\n' +
                        '                               placeholder="Press enter to post comment">\n' +
                        '                        <button onclick="addComment('+post["id"]+')" type="button">\n' +
                        '                            submit</button>\n' +
                        '\n' +
                        '                    </div>\n' +
                        '                </div>';
                    $('#'+bookId).append(comments);
                }
            });
        }
        function editComment(commentId , text) {
            var img = '/uploads/users/'+"{{Auth::user()->image}}";
            $('#comment'+commentId).ht$('#like-'+id).html('<i class="fa fa-thumbs-o-up"></i>'+
                        data['liked']);
                    $('#likes-comments-counter-'+id).html(data['likes']+" likes - "
                        +data['comments']+" comments");ml('</div>\n' +
                '                <!-- /.box-footer -->\n' +
                '                <div class="box-footer">\n' +
                '                    <img class="img-responsive img-circle img-sm" src='+img+' alt="Alt Text">\n' +
                '                    <!-- .img-push is used to add margin to elements next to floating images -->\n' +
                '                    <div class="img-push">\n' +
                '                        <input name="content"\n' +
                '                               id="input-comment-'+commentId+'"\n' +
                '                               type="text" class="form-control input-sm"\n' +
                '                               value="'+text+'">\n' +
                '                        <button onclick="updateComment('+commentId+')" type="button">\n' +
                '                            submit</button>\n' +
                '\n' +
                '                    </div>\n' +
                '                </div>');
        }
        function updateComment(commentId) {
            $.ajaxSetup({
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    }
                },
            });
            var token="{{csrf_token()}}";
            var URL = "{{url('api/comment')}}"+"/"+commentId;
            var content = $('#input-comment-'+commentId).val();
            $.ajax({
                type:'patch',
                url:URL,
                data: {
                    '_token': token,
                    'content': content,
                },
                success:function() {
                    var img = '/uploads/users/'+"{{Auth::user()->image}}";
                    $('#comment'+commentId).html(
                    '    <img class="img-circle img-sm" src='+img+' alt="User Image">\n' +
                    '\n' +
                    '    <div class="comment-text">\n' +
                    '                      <span class="username">\n' +
                    "{{Auth::user()->name}}"+ '\n' +
                    '                      </span>\n' +
                    content+'\n' +
                    '    </div>\n' +
                    '\n <a style="cursor: pointer" onclick="editComment('+commentId+',\''+content+'\')">edit</a> '+
                    '<a style="cursor: pointer" onclick="deleteComment('+commentId+')"> delete</a> '
                    );
                }
            });
        }
        function deleteComment(id) {
            $('#comment'+id).slideUp();
            var URL = "{{url('api/comment')}}"+"/"+id;
            console.log(URL);
            $.ajax({
                type:'delete',
                url:URL,
                success:function(data) {
                    console.log(data);
                    $('#likes-comments-counter-'+data['book_id']).html(data['likes']+" likes - "
                        +data['comments']+" comments");
                }
            });
        }
    </script>
    <script>
        @include('AjaxForCommentsAndLikes')
    </script>
@endsection
