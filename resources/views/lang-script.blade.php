<script>
    $(document).ready(function () {
        var lang = "{{session()->get('locale')}}";
        if(lang===""){
            lang="en";
        }
        $('#lang-select').val(lang);
        $('#lang-select').change(function () {
            var url = "{{url('/lang')}}"+"/"+$('#lang-select').val();
            $(location).attr('href',url);
        });
    })
</script>