@component('mail::message')
# {{$book->name}}

{{$book->name}} is out of stock comeback and add some of it
@component('mail::button', ['url' => "/admin/books/{$book->id}/edit"])
Add
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
