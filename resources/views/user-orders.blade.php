@extends('layouts.layout')
@push('sidebar')
    <li><a href="/user/home">{{__('msg.content')}}</a></li>
    <li><a href="/user/orders">{{__('msg.my_orders')}}</a></li>
@endpush
@section('content')

    <section class="content">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{__('msg.orders')}}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>{{__('msg.name')}}</th>
                        <th>{{__('msg.description')}}</th>
                        <th>{{__('msg.price')}}</th>
                        <th>{{__('msg.quantity')}}</th>
                        <th>{{__('msg.ordered_at')}}</th>
                    </tr>
                    </thead>
                    <tbody id="table-body">
                    <tfoot>
                    <tr>
                        <th>{{__('msg.name')}}</th>
                        <th>{{__('msg.description')}}</th>
                        <th>{{__('msg.price')}}</th>
                        <th>{{__('msg.quantity')}}</th>
                        <th>{{__('msg.ordered_at')}}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
@endsection
@section('script')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- jQuery 3 -->
    <script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{asset('backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('backend/bower_components/fastclick/lib/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('backend/dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
        jQuery(document).ready(function($) {
            $.ajaxSetup({
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {
                        xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                    }
                },
            });
            var token="{{csrf_token()}}";
            var access_key="{{Auth::user()->access_key}}";
            var userId="{{Auth::user()->id}}";
            var URL = "{{url('api/orders')}}"+"/"+userId+"/access_key="+access_key;
            console.log(URL);
            $.ajax({
                type: 'GET',
                url: URL,
                data: {
                    '_token': token,
                },
                success: function (data) {
                    for(var i=0 ; i<data["data"].length ; i++) {
                        var order = data["data"][i];
                        $('#table-body').append(
                            '<tr>\n' +
                            '                            <th>'+order["name"]+'</th>\n' +
                            '                            <th>'+order["description"]+'</th>\n' +
                            '                            <th>'+order["price"]+'</th>\n' +
                            '                            <th>'+order["quantity"]+'</th>\n' +
                            '                            <th>'+order["ordered_at"]+'</th>\n' +
                            '                            </tr>');
                    }
                }
            });
        });
    </script>
@endsection

