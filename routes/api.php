<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------|
| API Routes                                                               |
|--------------------------------------------------------------------------|
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'admin','namespace'=>'AdminsControllers'],function(){
   Route::group(['prefix'=>'users'],function (){
        Route::get('/','UsersController@getUsers')->middleware('auth:admin');
        Route::patch('/{user}','UsersController@editUser')->middleware('auth:admin');
        Route::delete('/{user}','UsersController@deleteUser')->middleware('auth:admin');
        Route::post('/create','UsersController@postUser')->middleware('auth:admin');
    });
    Route::group(['prefix'=>'books'],function(){
        Route::get('/','BooksController@getBooks')->middleware('auth:admin');
        Route::get('/{book}/edit','BooksController@getEditBook')->middleware('auth:admin');
        Route::patch('/{book}','BooksController@editBook')->middleware('auth:admin');
        Route::delete('/{book}','BooksController@deleteBook')->middleware('auth:admin');
        Route::post('/create','BooksController@postBook')->middleware('auth:admin');
        Route::get('/create','BooksController@addBook')->middleware('auth:admin');
    });
    Route::group(['prefix'=>'admins'],function(){
        Route::get('/','AdminController@getAdmins')->middleware('auth:admin');
        Route::get('/{admin}/edit','AdminController@getEditAdmin')->middleware('auth:admin');
        Route::patch('/{admin}','AdminController@editAdmin')->middleware('auth:admin');
        Route::delete('/{admin}','AdminController@deleteAdmin')->middleware('auth:admin');
        Route::post('/create','AdminController@postAdmin')->middleware('auth:admin');
        Route::get('/create','AdminController@addAdmin')->middleware('auth:admin');
    });
    Route::group(['prefix'=>'categories'],function(){
        Route::get('/','CategoriesController@getCategories')->middleware('auth:admin');
        Route::get('/{category}/edit','CategoriesController@getEditCategory')->middleware('auth:admin');
        Route::patch('/{category}','CategoriesController@editCategory')->middleware('auth:admin');
        Route::delete('/{category}','CategoriesController@deleteCategory')->middleware('auth:admin');
        Route::post('/create','CategoriesController@postCategory')->middleware('auth:admin');
        Route::get('/create','CategoriesController@addCategory')->middleware('auth:admin');
    });
});

Route::get('/books/{user}/{offset}','APIs\BooksAPI@index');
Route::get('/comments/{id}','APIs\commentsAPI@index');
Route::post('/register','APIs\AuthController@register');
Route::patch('/comment/{id}','UsersControllers\CommentsController@update');
Route::delete('/comment/{id}','UsersControllers\CommentsController@delete');
Route::get('/orders/{user}','APIs\OrdersAPI@get');
Route::get('/admins/access_key={apikey}','APIs\AdminsAPI@index')->middleware('APIkey');
Route::get('/users/access_key={apikey}','APIs\UsersAPI@index')->middleware('APIkey');
Route::get('/{admin_id}/categories/access_key={apikey}','APIs\CategoriesAPI@get')->middleware('APIkey');
Route::get('/{category_id}/books/access_key={apikey}','APIs\BooksAPI@get')->middleware('APIkey');
Route::get('/books/{user}/{index}/access_key={apikey}','APIs\BooksAPI@index')->middleware('APIkey');
Route::get('/orders/{user}/access_key={apikey}','APIs\OrdersAPI@get')->middleware('APIkey');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
