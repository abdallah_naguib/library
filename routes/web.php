<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/user/home');
});

Route::group(['prefix'=>'admin','namespace'=>'AdminsControllers'],function(){
    Route::post('/order','AdminController@order')->middleware('auth:admin');
    Route::post('/logn','AdminsAuthController@PostAdminLogin')->middleware('guest:admin');
    Route::get('/','AdminController@index')->middleware('auth:admin');
    Route::get('/login','AdminsAuthController@getAdminLogin')->middleware('guest');
    Route::get('/task','AdminController@task')->middleware('auth:admin');
    Route::group(['prefix'=>'users'],function (){
        Route::get('/','UsersController@getUsers')->middleware('auth:admin');
        Route::get('/create','UsersController@addUser')->middleware('auth:admin');
        Route::get('/{user}/edit','UsersController@getEditUser')->middleware('auth:admin');
        Route::patch('/{user}','UsersController@editUser')->middleware('auth:admin');
        Route::delete('/{user}','UsersController@deleteUser')->middleware('auth:admin');
        Route::post('/create','UsersController@postUser')->middleware('auth:admin');
        Route::get('/create','UsersController@addUser')->middleware('auth:admin');
        Route::post('/active','UsersController@active')->middleware('auth:admin');
    });
    Route::group(['prefix'=>'books'],function(){
        Route::get('/','BooksController@getBooks')->middleware('auth:admin');
        Route::get('/{book}/edit','BooksController@getEditBook')->middleware('auth:admin');
        Route::patch('/{book}','BooksController@editBook')->middleware('auth:admin');
        Route::delete('/{book}','BooksController@deleteBook')->middleware('auth:admin');
        Route::post('/create','BooksController@postBook')->middleware('auth:admin');
        Route::get('/create','BooksController@addBook')->middleware('auth:admin');
        Route::post('/active','BooksController@active')->middleware('auth:admin');
    });
    Route::group(['prefix'=>'admins'],function(){
        Route::get('/','AdminController@getAdmins')->middleware('auth:admin');
        Route::get('/{admin}/edit','AdminController@getEditAdmin')->middleware('auth:admin');
        Route::patch('/{admin}','AdminController@editAdmin')->middleware('auth:admin');
        Route::delete('/{admin}','AdminController@deleteAdmin')->middleware('auth:admin');
        Route::post('/create','AdminController@postAdmin')->middleware('auth:admin');
        Route::get('/create','AdminController@addAdmin')->middleware('auth:admin');
        Route::post('/active','AdminController@active')->middleware('auth:admin');
    });
    Route::group(['prefix'=>'categories'],function(){
        Route::get('/','CategoriesController@getCategories')->middleware('auth:admin');
        Route::get('/{category}/edit','CategoriesController@getEditCategory')->middleware('auth:admin');
        Route::patch('/{category}','CategoriesController@editCategory')->middleware('auth:admin');
        Route::delete('/{category}','CategoriesController@deleteCategory')->middleware('auth:admin');
        Route::post('/create','CategoriesController@postCategory')->middleware('auth:admin');
        Route::get('/create','CategoriesController@addCategory')->middleware('auth:admin');
        Route::post('/active','CategoriesController@active')->middleware('auth:admin');
    });
});
Route::group(['prefix'=>'user' , 'namespace'=>'UsersControllers'],function (){
    Route::group(['prefix'=>'books/{books}'],function(){
        Route::post('/like','LikesController@create')->middleware('auth');
        Route::post('/comment','CommentsController@create')->middleware('auth');
        Route::get('/show','BooksForUserController@show')->middleware('auth');
        Route::post('/order','BooksForUserController@order')->middleware('auth');
    });
    Route::get('/register','AuthController@getRegister')->middleware('guest');
    Route::post('/regster','AuthController@postRegister')->middleware('guest');
    Route::get('/login','AuthController@getLogin')->middleware('guest');
    Route::post('/logn','AuthController@PostLogin')->middleware('guest');
    Route::get('/home', 'HomeController@index')->middleware('auth');
    Route::get('/orders', 'HomeController@orders')->middleware('auth');
});
Auth::routes();
Route::get('/lang/{key}', function ($key) {
    Session::put('locale', $key);
    return redirect()->back();
});
